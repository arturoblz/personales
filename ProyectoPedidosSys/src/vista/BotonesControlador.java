package vista;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import vista.*;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class BotonesControlador implements Initializable {
    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
    @FXML
   private void mostrarArticulosFamilias(ActionEvent event) throws IOException{
    Parent vista = FXMLLoader.load(getClass().getResource("ArticulosFamiliasVista.fxml"));
    Scene sceneaf = new Scene(vista);
    Stage window = new Stage();
    window.setScene(sceneaf);
    window.show();
    }

    @FXML
    private void mostrarArticulos(ActionEvent event) throws IOException{
        Parent vista = FXMLLoader.load(getClass().getResource("ArticulosV.fxml"));
        Scene sceneaf = new Scene(vista);
        Stage window = new Stage();
        window.setScene(sceneaf);
        window.show();
    }
    @FXML
    private void mostrarProveedores(ActionEvent event) throws IOException{
        Parent vista = FXMLLoader.load(getClass().getResource("ProveedoresVista.fxml"));
        Scene sceneaf = new Scene(vista);
        Stage window = new Stage();
        window.setScene(sceneaf);
        window.show();
    }
    @FXML
    private void mostrarPedidos(ActionEvent event) throws IOException{
        Parent vista = FXMLLoader.load(getClass().getResource("PedidosVista.fxml"));
        Scene sceneaf = new Scene(vista);
        Stage window = new Stage();
        window.setScene(sceneaf);
        window.show();
    }
    @FXML
    private void mostrarPedidosLineas(ActionEvent event) throws IOException{
        Parent vista = FXMLLoader.load(getClass().getResource("PedidosLineasVista.fxml"));
        Scene sceneaf = new Scene(vista);
        Stage window = new Stage();
        window.setScene(sceneaf);
        window.show();
    }

}
