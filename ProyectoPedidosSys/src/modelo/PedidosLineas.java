package modelo;


import javafx.beans.property.*;
import javafx.collections.ObservableList;

import java.sql.*;

public class PedidosLineas{
    private IntegerProperty id;
    private IntegerProperty pedido_id;
    private IntegerProperty articulo_id;
    private StringProperty articulo_codigo;
    private StringProperty articulo_detalle;
    private IntegerProperty cant;
    private DoubleProperty precio_unitario;
    private DoubleProperty precio_total;
    private IntegerProperty unidades_servidas;
    private StringProperty nombrepedido;
    private  StringProperty nombrearticulo;


    public PedidosLineas(int id, int pedido_id, int articulo_id,
                         String articulo_codigo, String articulo_detalle, int cant,
                         Double precio_unitario, Double precio_total, int unidades_servidas) {
        this.id = new SimpleIntegerProperty(id);
        this.pedido_id = new SimpleIntegerProperty(pedido_id);
        this.articulo_id = new SimpleIntegerProperty(articulo_id);
        this.articulo_codigo = new SimpleStringProperty(articulo_codigo);
        this.articulo_detalle = new SimpleStringProperty(articulo_detalle);
        this.cant = new SimpleIntegerProperty(cant);
        this.precio_unitario = new SimpleDoubleProperty(precio_unitario);
        this.precio_total = new SimpleDoubleProperty(precio_total);
        this.unidades_servidas = new SimpleIntegerProperty(unidades_servidas);
    }
    public PedidosLineas(int id, int pedido_id, int articulo_id,
                         String articulo_codigo, String articulo_detalle, int cant,
                         Double precio_unitario, Double precio_total, int unidades_servidas, String nombrearticulo) {
        this.id = new SimpleIntegerProperty(id);
        this.pedido_id = new SimpleIntegerProperty(pedido_id);
        this.articulo_id = new SimpleIntegerProperty(articulo_id);
        this.articulo_codigo = new SimpleStringProperty(articulo_codigo);
        this.articulo_detalle = new SimpleStringProperty(articulo_detalle);
        this.cant = new SimpleIntegerProperty(cant);
        this.precio_unitario = new SimpleDoubleProperty(precio_unitario);
        this.precio_total = new SimpleDoubleProperty(precio_total);
        this.unidades_servidas = new SimpleIntegerProperty(unidades_servidas);

        this.nombrearticulo = new SimpleStringProperty(nombrearticulo);

    }

    //Metodos atributo: id
    public int getId() {
        return id.get();
    }
    public void setId(int id) {
        this.id = new SimpleIntegerProperty(id);
    }
    public IntegerProperty IdProperty() {
        return id;
    }
    //Metodos atributo: pedido_id
    public int getPedido_id() {
        return pedido_id.get();
    }
    public void setPedido_id(int pedido_id) {
        this.pedido_id = new SimpleIntegerProperty(pedido_id);
    }
    public IntegerProperty Pedido_idProperty() {
        return pedido_id;
    }
    //Metodos atributo: articulo_id
    public int getArticulo_id() {
        return articulo_id.get();
    }
    public void setArticulo_id(int articulo_id) {
        this.articulo_id = new SimpleIntegerProperty(articulo_id);
    }
    public IntegerProperty Articulo_idProperty() {
        return articulo_id;
    }
    //Metodos atributo: articulo_codigo
    public String getArticulo_codigo() {
        return articulo_codigo.get();
    }
    public void setArticulo_codigo(String articulo_codigo) {
        this.articulo_codigo = new SimpleStringProperty(articulo_codigo);
    }
    public StringProperty Articulo_codigoProperty() {
        return articulo_codigo;
    }
    //Metodos atributo: articulo_detalle
    public String getArticulo_detalle() {
        return articulo_detalle.get();
    }
    public void setArticulo_detalle(String articulo_detalle) {
        this.articulo_detalle = new SimpleStringProperty(articulo_detalle);
    }
    public StringProperty Articulo_detalleProperty() {
        return articulo_detalle;
    }
    //Metodos atributo: cant
    public int getCant() {
        return cant.get();
    }
    public void setCant(int cant) {
        this.cant = new SimpleIntegerProperty(cant);
    }
    public IntegerProperty CantProperty() {
        return cant;
    }
    //Metodos atributo: precio_unitario
    public Double getPrecio_unitario() {
        return precio_unitario.get();
    }
    public void setPrecio_unitario(Double precio_unitario) {
        this.precio_unitario = new SimpleDoubleProperty(precio_unitario);
    }
    public DoubleProperty Precio_unitarioProperty() {
        return precio_unitario;
    }
    //Metodos atributo: precio_total
    public Double getPrecio_total() {
        return precio_total.get();
    }
    public void setPrecio_total(Double precio_total) {
        this.precio_total = new SimpleDoubleProperty(precio_total);
    }
    public DoubleProperty Precio_totalProperty() {
        return precio_total;
    }
    //Metodos atributo: unidades_servidas
    public int getUnidades_servidas() {
        return unidades_servidas.get();
    }
    public void setUnidades_servidas(int unidades_servidas) {
        this.unidades_servidas = new SimpleIntegerProperty(unidades_servidas);
    }
    public IntegerProperty Unidades_servidasProperty() {
        return unidades_servidas;
    }


    public String getNombrepedido() {
        return nombrepedido.get();
    }

    public StringProperty nombrepedidoProperty() {
        return nombrepedido;
    }

    public void setNombrepedido(String nombrepedido) {
        this.nombrepedido.set(nombrepedido);
    }

    public String getNombrearticulo() {
        return nombrearticulo.get();
    }

    public StringProperty nombrearticuloProperty() {
        return nombrearticulo;
    }

    public void setNombrearticulo(String nombrearticulo) {
        this.nombrearticulo.set(nombrearticulo);
    }

    public int guardarLineas(Connection connection){
    try {
        //Evitar inyeccion SQL.
        PreparedStatement instruccion =
                connection.prepareStatement("INSERT INTO pedidos_lineas (id, pedido_id,articulo_id,articulo_codigo,articulo_detalle,cant,precio_unitario,precio_total,unidades_servidas) "+
                        "VALUES (0, ?,?,?,?,?,?,?,?)");
        instruccion.setInt(1,pedido_id.get());
        instruccion.setInt(2,articulo_id.get());
        instruccion.setString(3,articulo_codigo.get());
        instruccion.setString(4,articulo_detalle.get());
        instruccion.setDouble(5,cant.get());
        instruccion.setDouble(6,precio_unitario.get());
        instruccion.setDouble(7,precio_total.get());
        instruccion.setInt(8,unidades_servidas.get());




        return instruccion.executeUpdate();
    } catch (SQLException e) {
        e.printStackTrace();
        return 0;
    }


}
public  int eliminarLineas(Connection connection){
    try {
        PreparedStatement instruccion = connection.prepareStatement(
                "DELETE FROM pedidos_lineas "+
                        "WHERE id = ?"
        );
        instruccion.setInt(1, id.get());
        return instruccion.executeUpdate();
    } catch (SQLException e) {
        e.printStackTrace();
        return 0;
    }
}
public int actualizarLineas(Connection connection){
   try {
        PreparedStatement instruccion =
                connection.prepareStatement(
                        "UPDATE pedidos_lineas SET pedido_id = ?, articulo_id = ?, articulo_codigo = ?,articulo_detalle = ?, cant = ?, precio_unitario =?, precio_total=?, unidades_servidas=?  WHERE id = ?"
                );
       instruccion.setInt(1, pedido_id.get());
       instruccion.setInt(2, articulo_id.get());
       instruccion.setString(3, articulo_codigo.get());
       instruccion.setString(4, articulo_detalle.get());
       instruccion.setDouble(5, cant.get());
       instruccion.setDouble(6, precio_unitario.get());
       instruccion.setDouble(7, precio_total.get());
       instruccion.setInt(8, unidades_servidas.get());
       instruccion.setInt(9, id.get());


        return instruccion.executeUpdate();

    } catch (SQLException e) {
        e.printStackTrace();
        return 0;
    }
}
    public static void llenarInformacionPedidosLineas(Connection connection, ObservableList<PedidosLineas> listaPedidosLineas) {
        try {
            Statement instruccion = connection.createStatement();
            ResultSet resultado = instruccion.executeQuery(

                    "SELECT id, pedido_id, articulo_id,articulo_codigo,articulo_detalle,cant,precio_unitario,precio_total,unidades_servidas from plview"


            );
            while (resultado.next()) {
                listaPedidosLineas.add(new PedidosLineas(
                        resultado.getInt("id"),
                        resultado.getInt("pedido_id"),
                        resultado.getInt("articulo_id"),
                        resultado.getString("articulo_codigo"),
                        resultado.getString("articulo_detalle"),
                        resultado.getInt("cant"),
                        resultado.getDouble("precio_unitario"),
                        resultado.getDouble("precio_total"),
                        resultado.getInt("unidades_servidas")

                ));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public static void refresT(ObservableList<PedidosLineas> lista,Connection connection){
        lista.clear();
        try {
            Statement instruccion = connection.createStatement();
            ResultSet resultado = instruccion.executeQuery(
                    "SELECT A.id, A.pedido_id, A.articulo_id,A.articulo_codigo,A.articulo_detalle,A.cant,A.precio_unitario,A.precio_total,A.unidades_servidas, B.nombre as nombrearticulo FROM pedidos_lineas A " +
                            "LEFT JOIN articulos B " +
                            "ON (A.pedido_id = B.id) "



            );
            while (resultado.next()) {
                lista.add(new PedidosLineas(
                        resultado.getInt("id"),
                        resultado.getInt("pedido_id"),
                        resultado.getInt("articulo_id"),
                        resultado.getString("articulo_codigo"),
                        resultado.getString("articulo_detalle"),
                        resultado.getInt("cant"),
                        resultado.getDouble("precio_unitario"),
                        resultado.getDouble("precio_total"),
                        resultado.getInt("unidades_servidas"),
                        resultado.getString("nombrearticulo")
                ));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


}