package modelo;


import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import sun.plugin.perf.PluginRollup;

import java.sql.*;

public class Proveedores{
    private IntegerProperty id;
    private StringProperty razon_social;
    private StringProperty nombre;
    private StringProperty nif;
    private StringProperty direccion;
    private StringProperty codigo_postal;
    private StringProperty poblacion;
    private StringProperty provincia;
    private StringProperty pais;
    private StringProperty tel;
    private StringProperty movil;
    private StringProperty fax;
    private StringProperty email;
    private StringProperty web;
    private IntegerProperty activo;
    private StringProperty obs;
    private StringProperty contacto;

    public Proveedores(int id, String razon_social, String nombre,
                       String nif, String direccion, String codigo_postal,
                       String poblacion, String provincia, String pais,
                       String tel, String movil, String fax,
                       String email, String web, int activo,
                       String obs, String contacto) {
        this.id = new SimpleIntegerProperty(id);
        this.razon_social = new SimpleStringProperty(razon_social);
        this.nombre = new SimpleStringProperty(nombre);
        this.nif = new SimpleStringProperty(nif);
        this.direccion = new SimpleStringProperty(direccion);
        this.codigo_postal = new SimpleStringProperty(codigo_postal);
        this.poblacion = new SimpleStringProperty(poblacion);
        this.provincia = new SimpleStringProperty(provincia);
        this.pais = new SimpleStringProperty(pais);
        this.tel = new SimpleStringProperty(tel);
        this.movil = new SimpleStringProperty(movil);
        this.fax = new SimpleStringProperty(fax);
        this.email = new SimpleStringProperty(email);
        this.web = new SimpleStringProperty(web);
        this.activo = new SimpleIntegerProperty(activo);
        this.obs = new SimpleStringProperty(obs);
        this.contacto = new SimpleStringProperty(contacto);
    }

    //Metodos atributo: id
    public int getId() {
        return id.get();
    }
    public void setId(int id) {
        this.id = new SimpleIntegerProperty(id);
    }
    public IntegerProperty IdProperty() {
        return id;
    }
    //Metodos atributo: razon_social
    public String getRazon_social() {
        return razon_social.get();
    }
    public void setRazon_social(String razon_social) {
        this.razon_social = new SimpleStringProperty(razon_social);
    }
    public StringProperty Razon_socialProperty() {
        return razon_social;
    }
    //Metodos atributo: nombre
    public String getNombre() {
        return nombre.get();
    }
    public void setNombre(String nombre) {
        this.nombre = new SimpleStringProperty(nombre);
    }
    public StringProperty NombreProperty() {
        return nombre;
    }
    //Metodos atributo: nif
    public String getNif() {
        return nif.get();
    }
    public void setNif(String nif) {
        this.nif = new SimpleStringProperty(nif);
    }
    public StringProperty NifProperty() {
        return nif;
    }
    //Metodos atributo: direccion
    public String getDireccion() {
        return direccion.get();
    }
    public void setDireccion(String direccion) {
        this.direccion = new SimpleStringProperty(direccion);
    }
    public StringProperty DireccionProperty() {
        return direccion;
    }
    //Metodos atributo: codigo_postal
    public String getCodigo_postal() {
        return codigo_postal.get();
    }
    public void setCodigo_postal(String codigo_postal) {
        this.codigo_postal = new SimpleStringProperty(codigo_postal);
    }
    public StringProperty Codigo_postalProperty() {
        return codigo_postal;
    }
    //Metodos atributo: poblacion
    public String getPoblacion() {
        return poblacion.get();
    }
    public void setPoblacion(String poblacion) {
        this.poblacion = new SimpleStringProperty(poblacion);
    }
    public StringProperty PoblacionProperty() {
        return poblacion;
    }
    //Metodos atributo: provincia
    public String getProvincia() {
        return provincia.get();
    }
    public void setProvincia(String provincia) {
        this.provincia = new SimpleStringProperty(provincia);
    }
    public StringProperty ProvinciaProperty() {
        return provincia;
    }
    //Metodos atributo: pais
    public String getPais() {
        return pais.get();
    }
    public void setPais(String pais) {
        this.pais = new SimpleStringProperty(pais);
    }
    public StringProperty PaisProperty() {
        return pais;
    }
    //Metodos atributo: tel
    public String getTel() {
        return tel.get();
    }
    public void setTel(String tel) {
        this.tel = new SimpleStringProperty(tel);
    }
    public StringProperty TelProperty() {
        return tel;
    }
    //Metodos atributo: movil
    public String getMovil() {
        return movil.get();
    }
    public void setMovil(String movil) {
        this.movil = new SimpleStringProperty(movil);
    }
    public StringProperty MovilProperty() {
        return movil;
    }
    //Metodos atributo: fax
    public String getFax() {
        return fax.get();
    }
    public void setFax(String fax) {
        this.fax = new SimpleStringProperty(fax);
    }
    public StringProperty FaxProperty() {
        return fax;
    }
    //Metodos atributo: email
    public String getEmail() {
        return email.get();
    }
    public void setEmail(String email) {
        this.email = new SimpleStringProperty(email);
    }
    public StringProperty EmailProperty() {
        return email;
    }
    //Metodos atributo: web
    public String getWeb() {
        return web.get();
    }
    public void setWeb(String web) {
        this.web = new SimpleStringProperty(web);
    }
    public StringProperty WebProperty() {
        return web;
    }
    //Metodos atributo: activo
    public int getActivo() {
        return activo.get();
    }
    public void setActivo(int activo) {
        this.activo = new SimpleIntegerProperty(activo);
    }
    public IntegerProperty ActivoProperty() {
        return activo;
    }
    //Metodos atributo: obs
    public String getObs() {
        return obs.get();
    }
    public void setObs(String obs) {
        this.obs = new SimpleStringProperty(obs);
    }
    public StringProperty ObsProperty() {
        return obs;
    }
    //Metodos atributo: contacto
    public String getContacto() {
        return contacto.get();
    }
    public void setContacto(String contacto) {
        this.contacto = new SimpleStringProperty(contacto);
    }
    public StringProperty ContactoProperty() {
        return contacto;
    }

    public int guardarProveedores(Connection connection){
        try {
            //Evitar inyeccion SQL.
            PreparedStatement instruccion =
                    connection.prepareStatement("INSERT INTO proveedores (id, razon_social, nombre,nif,direccion,codigo_postal,poblacion,provincia,pais,tel,movil,fax,email,web,activo,obs,contacto) "+
                            "VALUES (0, ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

            instruccion.setString(1,razon_social.get());
            instruccion.setString(2,nombre.get());
            instruccion.setString(3,nif.get());
            instruccion.setString(4,direccion.get());
            instruccion.setString(5,codigo_postal.get());
            instruccion.setString(6,poblacion.get());
            instruccion.setString(7,provincia.get());
            instruccion.setString(8,pais.get());
            instruccion.setString(9,tel.get());
            instruccion.setString(10,movil.get());
            instruccion.setString(11,fax.get());
            instruccion.setString(12,email.get());
            instruccion.setString(13,web.get());
            instruccion.setInt(14,activo.get());
            instruccion.setString(15,obs.get());
            instruccion.setString(16,contacto.get());





            return instruccion.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public int actualizarProveedores(Connection connection) {
        try {
            PreparedStatement instruccion =
                    connection.prepareStatement(
                            "UPDATE proveedores SET razon_social = ?, nombre = ?,nif =?,direccion =?,codigo_postal=?,poblacion =?,provincia=?,pais=?,tel=?,movil=?,fax=?,email=?,web=?,activo=?,obs=?,contacto=?  WHERE id = ?"
                    );
            instruccion.setString(1, razon_social.get());
            instruccion.setString(2, nombre.get());
            instruccion.setString(3, nif.get());
            instruccion.setString(4, direccion.get());
            instruccion.setString(5, codigo_postal.get());
            instruccion.setString(6, poblacion.get());
            instruccion.setString(7, provincia.get());
            instruccion.setString(8, pais.get());
            instruccion.setString(9, tel.get());
            instruccion.setString(10, movil.get());
            instruccion.setString(11, fax.get());
            instruccion.setString(12, email.get());
            instruccion.setString(13, web.get());
            instruccion.setInt(14, activo.get());
            instruccion.setString(15, obs.get());
            instruccion.setString(16, contacto.get());
            instruccion.setInt(17, id.get());
            return instruccion.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }

    }


    public int eliminarProveedores(Connection connection){
        try {
            PreparedStatement instruccion = connection.prepareStatement(
                    "DELETE FROM proveedores "+
                            "WHERE id = ?"
            );
            instruccion.setInt(1, id.get());
            return instruccion.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }




    public static void llenarInformacionProveedores(Connection connection, ObservableList<Proveedores> listaP){
        try {
            Statement instruccion = connection.createStatement();
            ResultSet resultado = instruccion.executeQuery(
                    "SELECT id, razon_social, nombre,nif,direccion,codigo_postal,poblacion,provincia,pais,tel,movil,fax,email,web,activo,obs,contacto  FROM proveedores"
            );
            while(resultado.next()){
                listaP.add(new Proveedores(
                        resultado.getInt("id"),
                        resultado.getString("razon_social"),
                        resultado.getString("nombre"),
                        resultado.getString("nif"),
                        resultado.getString("direccion"),
                        resultado.getString("codigo_postal"),
                        resultado.getString("poblacion"),
                        resultado.getString("provincia"),
                        resultado.getString("pais"),
                        resultado.getString("tel"),
                        resultado.getString("movil"),
                        resultado.getString("fax"),
                        resultado.getString("email"),
                        resultado.getString("web"),
                        resultado.getInt("activo"),
                        resultado.getString("obs"),
                        resultado.getString("contacto")
                ));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

    @Override
    public String toString() {
        return nombre.get();

    }
    public static void refresT(ObservableList<Proveedores> lista, Connection connection){
        lista.clear();
        try {
            Statement instruccion = connection.createStatement();
            ResultSet resultado = instruccion.executeQuery(
                    "SELECT id, razon_social, nombre,nif,direccion,codigo_postal,poblacion,provincia,pais,tel,movil,fax,email,web,activo,obs,contacto  FROM proveedores"
            );
            while(resultado.next()){
                lista.add(new Proveedores(
                        resultado.getInt("id"),
                        resultado.getString("razon_social"),
                        resultado.getString("nombre"),
                        resultado.getString("nif"),
                        resultado.getString("direccion"),
                        resultado.getString("codigo_postal"),
                        resultado.getString("poblacion"),
                        resultado.getString("provincia"),
                        resultado.getString("pais"),
                        resultado.getString("tel"),
                        resultado.getString("movil"),
                        resultado.getString("fax"),
                        resultado.getString("email"),
                        resultado.getString("web"),
                        resultado.getInt("activo"),
                        resultado.getString("obs"),
                        resultado.getString("contacto")
                ));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
