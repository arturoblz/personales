package modelo;

import javafx.beans.property.*;
import javafx.collections.ObservableList;

import java.sql.*;

public class Articulos {

    private IntegerProperty id;
    private StringProperty codigo;
    private IntegerProperty familia_id;
    private StringProperty nombre;
    private StringProperty codigo_barras;
    private DoubleProperty importe;
    private StringProperty nombrefamilia;

    public Articulos(int id, String codigo, int familia_id,
                       String nombre, String codigo_barras, Double importe) {
        this.id = new SimpleIntegerProperty(id);
        this.codigo = new SimpleStringProperty(codigo);
        this.familia_id = new SimpleIntegerProperty(familia_id);
        this.nombre = new SimpleStringProperty(nombre);
        this.codigo_barras = new SimpleStringProperty(codigo_barras);
        this.importe = new SimpleDoubleProperty(importe);
    }
    public Articulos(int id, String codigo, int familia_id,
                     String nombre, String codigo_barras, Double importe,String nombrefamilia) {
        this.id = new SimpleIntegerProperty(id);
        this.codigo = new SimpleStringProperty(codigo);
        this.familia_id = new SimpleIntegerProperty(familia_id);
        this.nombre = new SimpleStringProperty(nombre);
        this.codigo_barras = new SimpleStringProperty(codigo_barras);
        this.importe = new SimpleDoubleProperty(importe);
        this.nombre = new SimpleStringProperty(nombrefamilia);
    }

    //Metodos atributo: id
    public int getId() {
        return id.get();
    }
    public void setId(int id) {
        this.id = new SimpleIntegerProperty(id);
    }
    public IntegerProperty IdProperty() {
        return id;
    }
    //Metodos atributo: codigo
    public String getCodigo() {
        return codigo.get();
    }
    public void setCodigo(String codigo) {
        this.codigo = new SimpleStringProperty(codigo);
    }
    public StringProperty CodigoProperty() {
        return codigo;
    }
    //Metodos atributo: familia_id
    public int getFamilia_id() {
        return familia_id.get();
    }
    public void setFamilia_id(int familia_id) {
        this.familia_id = new SimpleIntegerProperty(familia_id);
    }
    public IntegerProperty Familia_idProperty() {
        return familia_id;
    }
    //Metodos atributo: nombre
    public String getNombre() {
        return nombre.get();
    }
    public void setNombre(String nombre) {
        this.nombre = new SimpleStringProperty(nombre);
    }
    public StringProperty NombreProperty() {
        return nombre;
    }
    //Metodos atributo: codigo_barras
    public String getCodigo_barras() {
        return codigo_barras.get();
    }
    public void setCodigo_barras(String codigo_barras) {
        this.codigo_barras = new SimpleStringProperty(codigo_barras);
    }
    public StringProperty Codigo_barrasProperty() {
        return codigo_barras;
    }
    //Metodos atributo: importe
    public Double getImporte() {
        return importe.get();
    }
    public void setImporte(Double importe) {
        this.importe = new SimpleDoubleProperty(importe);
    }
    public DoubleProperty ImporteProperty() {
        return importe;
    }

    public String getNombrefamilia() {
        return nombrefamilia.get();
    }

    public StringProperty nombrefamiliaProperty() {
        return nombrefamilia;
    }

    public void setNombrefamilia(String nombrefamilia) {
        this.nombrefamilia.set(nombrefamilia);
    }

    public int guardarArticulo(Connection connection){
        try {
            //Evitar inyeccion SQL.
            PreparedStatement instruccion =
                    connection.prepareStatement("INSERT INTO articulos (id,codigo,familia_id,nombre,codigo_barras,importe) "+
                            "VALUES (0, ?,?,?,?,?)");
            instruccion.setString(1,codigo.get());
            instruccion.setInt(2,familia_id.get());
            instruccion.setString(3,nombre.get());
            instruccion.setString(4,codigo_barras.get());
            instruccion.setDouble(5,importe.get());

            return instruccion.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public int eliminarArticulos(Connection connection){
        try {
            PreparedStatement instruccion = connection.prepareStatement(
                    "DELETE FROM articulos "+
                            "WHERE id = ?"
            );
            instruccion.setInt(1, id.get());
            return instruccion.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public int actualizarArticulos(Connection connection){
        try {
            PreparedStatement instruccion =
                    connection.prepareStatement(
                            "UPDATE articulos SET codigo=?, familia_id= ? , nombre = ?, codigo_barras =?, importe = ? WHERE id = ?"
                    );
            instruccion.setString(1, codigo.get());
            instruccion.setInt(2, familia_id.get());
            instruccion.setString(3, nombre.get());
            instruccion.setString(4, codigo_barras.get());
            instruccion.setDouble(5, importe.get());
            instruccion.setInt(6,id.get());
            return instruccion.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }

public static void llenarArticulos(Connection connection, ObservableList<Articulos> lista){
    try {
        Statement instruccion = connection.createStatement();
        ResultSet resultado = instruccion.executeQuery(
                "SELECT A.id, A.codigo, A.familia_id, A.nombre, A.codigo_barras, A.importe, B.nombre as nombref FROM articulos A LEFT JOIN articulos_familias B ON (A.familia_id = B.id) "
        );
        while(resultado.next()){
            lista.add(new Articulos(
                    resultado.getInt("id"),
                    resultado.getString("codigo"),
                    resultado.getInt("familia_id"),
                    resultado.getString("nombre"),
                    resultado.getString("codigo_barras"),
                    resultado.getDouble("importe"),
                    resultado.getString("nombref")


            ));
        }

    } catch (SQLException e) {
        e.printStackTrace();
    }

}

    public static void refresT(ObservableList<Articulos> lista,Connection connection){
        lista.clear();
        try {
            Statement instruccion = connection.createStatement();
            ResultSet resultado = instruccion.executeQuery(
                    "SELECT id, codigo, familia_id, nombre, codigo_barras, importe FROM articulos"
            );
            while(resultado.next()){
                lista.add(new Articulos(
                        resultado.getInt("id"),
                        resultado.getString("codigo"),
                        resultado.getInt("familia_id"),
                        resultado.getString("nombre"),
                        resultado.getString("codigo_barras"),
                        resultado.getDouble("importe")
                ));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
