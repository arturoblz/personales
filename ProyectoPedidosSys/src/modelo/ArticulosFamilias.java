package modelo;


import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;

import java.sql.*;

public class ArticulosFamilias{
    private IntegerProperty id;
    private StringProperty nombre;

    public ArticulosFamilias(int id, String nombre) {
        this.id = new SimpleIntegerProperty(id);
        this.nombre = new SimpleStringProperty(nombre);
    }

    //Metodos atributo: id
    public int getId() {
        return id.get();
    }
    public void setId(int id) {
        this.id = new SimpleIntegerProperty(id);
    }
    public IntegerProperty IdProperty() {
        return id;
    }
    //Metodos atributo: nombre
    public String getNombre() {
        return nombre.get();
    }
    public void setNombre(String nombre) {
        this.nombre = new SimpleStringProperty(nombre);
    }
    public StringProperty NombreProperty() {
        return nombre;
    }

    public int guardarRegistro(Connection connection){
        try {
            //Evitar inyeccion SQL.
            PreparedStatement instruccion =
                    connection.prepareStatement("INSERT INTO articulos_familias (id, nombre) "+
                            "VALUES (0, ?)");
           instruccion.setString(1,nombre.get());


            return instruccion.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }


    }
    public int eliminarRegistro(Connection connection){
        try {
            PreparedStatement instruccion = connection.prepareStatement(
                    "DELETE FROM articulos_familias "+
                            "WHERE id = ?"
            );
            instruccion.setInt(1, id.get());
            return instruccion.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }

    }
    public int actualizarRegistro(Connection connection){
        try {
            PreparedStatement instruccion =
                    connection.prepareStatement(
                            "UPDATE articulos_familias SET nombre = ? WHERE id = ?"
                    );
            instruccion.setString(1, nombre.get());
            instruccion.setInt(2,id.get());
            return instruccion.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }




    public static void llenarInformacionArticulosFamilias(Connection connection, ObservableList<ArticulosFamilias> lista){
        try {
            Statement instruccion = connection.createStatement();
            ResultSet resultado = instruccion.executeQuery(
                    "SELECT id, nombre FROM articulos_familias"
            );
            while(resultado.next()){
                lista.add(new ArticulosFamilias(
                        resultado.getInt("id"),
                        resultado.getString("nombre")
                ));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void refresT(ObservableList<ArticulosFamilias> lista,Connection connection){
        lista.clear();
        try {
            Statement instruccion = connection.createStatement();
            ResultSet resultado = instruccion.executeQuery(
                    "SELECT id, nombre FROM articulos_familias"
            );
            while(resultado.next()){
                lista.add(new ArticulosFamilias(
                        resultado.getInt("id"),
                        resultado.getString("nombre")
                ));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}