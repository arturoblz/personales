package modelo;


import javafx.beans.property.*;
import javafx.collections.ObservableList;

import java.sql.*;

public class Pedidos{
    private IntegerProperty id;
    private IntegerProperty proveedor_id;
    private Date fecha_pedido;
    private Date fecha_entraga;
    private IntegerProperty id_estado;
    private DoubleProperty total;
    private StringProperty nombre;
    private StringProperty nomestado;


    public Pedidos(int id, int proveedor_id, Date fecha_pedido,
                   Date fecha_entraga, int id_estado, Double total) {
        this.id = new SimpleIntegerProperty(id);
        this.proveedor_id = new SimpleIntegerProperty(proveedor_id);
        this.fecha_pedido = fecha_pedido;
        this.fecha_entraga = fecha_entraga;
        this.id_estado = new SimpleIntegerProperty(id_estado);
        this.total = new SimpleDoubleProperty(total);
    }

    public Pedidos(int id, int proveedor_id, Date fecha_pedido,
                   Date fecha_entraga, int id_estado, Double total, String nombre,String nomestado) {
        this.id = new SimpleIntegerProperty(id);
        this.proveedor_id = new SimpleIntegerProperty(proveedor_id);
        this.fecha_pedido = fecha_pedido;
        this.fecha_entraga = fecha_entraga;
        this.id_estado = new SimpleIntegerProperty(id_estado);
        this.total = new SimpleDoubleProperty(total);
        this.nombre = new SimpleStringProperty(nombre);
        this.nomestado = new SimpleStringProperty(nomestado);
    }

    //Metodos atributo: id
    public int getId() {
        return id.get();
    }
    public void setId(int id) {
        this.id = new SimpleIntegerProperty(id);
    }
    public IntegerProperty IdProperty() {
        return id;
    }
    //Metodos atributo: proveedor_id
    public int getProveedor_id() {
        return proveedor_id.get();
    }
    public void setProveedor_id(int proveedor_id) {
        this.proveedor_id = new SimpleIntegerProperty(proveedor_id);
    }
    public IntegerProperty Proveedor_idProperty() {
        return proveedor_id;
    }
    //Metodos atributo: fecha_pedido
    public Date getFecha_pedido() {
        return fecha_pedido;
    }
    public void setFecha_pedido(Date fecha_pedido) {
        this.fecha_pedido = fecha_pedido;
    }
    //Metodos atributo: fecha_entraga
    public Date getFecha_entraga() {
        return fecha_entraga;
    }
    public void setFecha_entraga(Date fecha_entraga) {
        this.fecha_entraga = fecha_entraga;
    }
    //Metodos atributo: estado
    public int getId_estado() {
        return id_estado.get();
    }
    public void setId_estado(int id_estado) {
        this.id_estado = new SimpleIntegerProperty(id_estado);
    }
    public IntegerProperty EstadoProperty() {
        return id_estado;
    }
    //Metodos atributo: total
    public Double getTotal() {
        return total.get();
    }
    public void setTotal(Double total) {
        this.total = new SimpleDoubleProperty(total);
    }
    public DoubleProperty TotalProperty() {
        return total;
    }

    public String getNombre() {
        return nombre.get();
    }

    public StringProperty nombreProperty() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre.set(nombre);
    }

    public String getNomestado() {
        return nomestado.get();
    }

    public StringProperty nomestadoProperty() {
        return nomestado;
    }

    public void setNomestado(String nomestado) {
        this.nomestado.set(nomestado);
    }

    @Override
    public String toString() {
        return "Pedido: " +
                 + id.get();
    }

    public int guardarPedido(Connection connection){

    try {
        //Evitar inyeccion SQL.
        PreparedStatement instruccion =
                connection.prepareStatement("INSERT INTO pedidos (id,proveedor_id,fecha_pedido,fecha_entraga,id_estado,total) "+
                        "VALUES (0, ?,?,?,?,?)");
        instruccion.setInt(1,proveedor_id.get());
        instruccion.setDate(2,Date.valueOf(fecha_pedido.toLocalDate()));
        instruccion.setDate(3,Date.valueOf(fecha_entraga.toLocalDate()));
        instruccion.setInt(4, id_estado.get());
        instruccion.setDouble(5,Double.valueOf(total.get()));



        return instruccion.executeUpdate();
    } catch (SQLException e) {
        e.printStackTrace();
        return 0;
    }
}
    public int eliminarPedido(Connection connection){
        try {
            PreparedStatement instruccion = connection.prepareStatement(
                    "DELETE FROM pedidos "+
                            "WHERE id = ?"
            );
            instruccion.setInt(1, id.get());
            return instruccion.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public int actualizarPedido(Connection connection){
        try {
            PreparedStatement instruccion =
                    connection.prepareStatement(
                            "UPDATE pedidos SET pedidos.proveedor_id = ?,fecha_pedido = ? , fecha_entraga = ?, estado = ?, total =?  WHERE id = ?"
                    );
            instruccion.setInt(1, proveedor_id.get());
            instruccion.setDate(2, Date.valueOf(fecha_pedido.toString()));
            instruccion.setDate(3, Date.valueOf(fecha_entraga.toLocalDate()));
            instruccion.setInt(4, id_estado.get());
            instruccion.setDouble(5, total.get());
            instruccion.setInt(6, proveedor_id.get());

            return instruccion.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("No se actulizo");
            return 0;
        }
    }


    public static void llenarInformacionPedidos(Connection connection, ObservableList<Pedidos> listaPedidos){
        try {
            Statement instruccion = connection.createStatement();
            ResultSet resultado = instruccion.executeQuery(
                    "SELECT A.id, A.proveedor_id, A.fecha_pedido,A.fecha_entraga,A.id_estado,A.total,B.nombre as nombre,C.nombre as nomestado FROM pedidos A LEFT JOIN proveedores B ON(A.proveedor_id = B.id ) LEFT JOIN estado C ON(A.id_estado = C.id_estado)"


            );
            while(resultado.next()){
                listaPedidos.add(new Pedidos(
                        resultado.getInt("id"),
                        resultado.getInt("proveedor_id"),
                        resultado.getDate("fecha_pedido"),
                        resultado.getDate("fecha_entraga"),
                        resultado.getInt("id_estado"),
                        resultado.getDouble("total"),
                        resultado.getString("nombre"),
                        resultado.getString("nomestado")
                ));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }


    }
    public static void refresT(ObservableList<Pedidos> lista,Connection connection){
        lista.clear();
        try {
            Statement instruccion = connection.createStatement();
            ResultSet resultado = instruccion.executeQuery(
                    "SELECT A.id, A.proveedor_id, A.fecha_pedido,A.fecha_entraga,A.id_estado,A.total,B.nombre as nombre,C.nombre as nomestado FROM pedidos A LEFT JOIN proveedores B ON(A.proveedor_id = B.id ) LEFT JOIN estado C ON(A.id_estado = C.id_estado)"


            );
            while(resultado.next()){
                lista.add(new Pedidos(
                        resultado.getInt("id"),
                        resultado.getInt("proveedor_id"),
                        resultado.getDate("fecha_pedido"),
                        resultado.getDate("fecha_entraga"),
                        resultado.getInt("id_estado"),
                        resultado.getDouble("total"),
                        resultado.getString("nombre"),
                        resultado.getString("nomestado")
                ));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}