package controller;


import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Duration;
import modelo.ArticulosFamilias;
import modelo.Conexion;

import java.net.URL;
import java.util.ResourceBundle;

public class ArticulosFamiliasControlador implements Initializable {
    @FXML
    private TextField txtId;
    @FXML
    private TextField txtNombre;
    @FXML
    private Button btnGuardar;
    @FXML
    private Button btnEliminar;
    @FXML
    private Button btnActualizar;
    @FXML
    private Button btnRefresh;



    @FXML
    private TableView<ArticulosFamilias> tableViewAF;
    @FXML
    private TableColumn<ArticulosFamilias,Integer> clmnId;
    @FXML
    private TableColumn<ArticulosFamilias,String> clmnNombre;

    private ObservableList<ArticulosFamilias> listaAF;

    private Conexion conexion;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        conexion = new Conexion() ;
        conexion.establecerConexion();
        listaAF = FXCollections.observableArrayList();
        ArticulosFamilias.llenarInformacionArticulosFamilias(conexion.getConnection(),listaAF);
        tableViewAF.setItems(listaAF);

        // Enlazando columnas con atributos
        clmnId.setCellValueFactory(new PropertyValueFactory<ArticulosFamilias,Integer>("id"));
        clmnNombre.setCellValueFactory(new PropertyValueFactory<ArticulosFamilias,String>("nombre"));
        gestionarEventos();
        conexion.cerrarConexion();

    }

    public void gestionarEventos(){
        tableViewAF.getSelectionModel().selectedItemProperty().addListener(
                new ChangeListener<ArticulosFamilias>() {
                    @Override
                    public void changed(ObservableValue<? extends ArticulosFamilias> arg0, ArticulosFamilias valorAnterior, ArticulosFamilias valorSeleccionado) {
                        if (valorSeleccionado!=null){
                            txtId.setText(String.valueOf(valorSeleccionado.getId()));
                            txtNombre.setText(valorSeleccionado.getNombre());


                            btnGuardar.setDisable(true);
                            btnEliminar.setDisable(false);
                            btnActualizar.setDisable(false);
                        }
                    }


                }
        );
    }

    @FXML
    public void actualizarRegistro(){
        ArticulosFamilias a = new ArticulosFamilias(Integer.valueOf(txtId.getText()),txtNombre.getText());
        conexion.establecerConexion();
        int resultado = a.actualizarRegistro(conexion.getConnection());
        conexion.cerrarConexion();

        if (resultado == 1){
            listaAF.set(tableViewAF.getSelectionModel().getSelectedIndex(),a);
            //JDK 8u>40
            Alert mensaje = new Alert(Alert.AlertType.INFORMATION);
            mensaje.setTitle("Registro actualizado");
            mensaje.setContentText("El registro ha sido actualizado exitosamente");
            mensaje.setHeaderText("Resultado:");
            mensaje.show();
        }
    }
    @FXML
    public void guardarRegistroArticulosFam(){
//Crear una nueva instancia del tipo Alumno
        ArticulosFamilias a = new ArticulosFamilias(0,
                txtNombre.getText());
        //Llamar al metodo guardarRegistro de la clase Alumno
        conexion.establecerConexion();
        int resultado = a.guardarRegistro(conexion.getConnection());
        conexion.cerrarConexion();

        if (resultado == 1){
            listaAF.add(a);
            //JDK 8u>40
            Alert mensaje = new Alert(Alert.AlertType.INFORMATION);
            mensaje.setTitle("Registro agregado");
            mensaje.setContentText("El registro ha sido agregado exitosamente");
            mensaje.setHeaderText("Resultado:");
            mensaje.show();
        }
    }
    @FXML
    public void eliminarRegistro(){
        conexion.establecerConexion();
        int resultado = tableViewAF.getSelectionModel().getSelectedItem().eliminarRegistro(conexion.getConnection());
        conexion.cerrarConexion();

        if (resultado == 1){
            listaAF.remove(tableViewAF.getSelectionModel().getSelectedIndex());
            //JDK 8u>40
            Alert mensaje = new Alert(Alert.AlertType.INFORMATION);
            mensaje.setTitle("Registro eliminado");
            mensaje.setContentText("El registro ha sido eliminado exitosamente");
            mensaje.setHeaderText("Resultado:");
            mensaje.show();
        }
    }

    @FXML
    public void limpiarComponentes(){
        txtId.setText(null);
        txtNombre.setText(null);


        btnGuardar.setDisable(false);
        btnEliminar.setDisable(true);
        btnActualizar.setDisable(true);
    }

    @FXML
    public void refreshAcccion(){
        conexion.establecerConexion();
        listaAF.clear();
        ArticulosFamilias.refresT(listaAF,conexion.getConnection());
        conexion.cerrarConexion();
    }
}
