package controller;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import modelo.Conexion;
import modelo.Pedidos;
import modelo.Proveedores;


import java.net.URL;
import java.sql.Date;
import java.util.ResourceBundle;

public class PedidosControlador implements Initializable {

    @FXML
    TextField textFieldIdP;
    @FXML
    TextField textFielProveedor;
    @FXML
    ToggleGroup toggleGroupestado;
    @FXML
    RadioButton radioButtonEnCurso;
    @FXML
    RadioButton radioButtonParcial;
    @FXML
    RadioButton radioButtonEntregado;




    @FXML
    DatePicker datePickerFechaPedido;
    @FXML
    DatePicker datePickerFechaEntrega;
    @FXML
    TextField textFieldEstado;
    @FXML
    TextField textFieldTotal;


    @FXML
    Button btnGuardar;
    @FXML
    Button btnActualizar;
    @FXML
    Button btnEliminar;


    @FXML
    TableView<Pedidos> tableViewPedidos;
    @FXML
    TableColumn<Pedidos, Integer> tableColumnId;

    @FXML
    TableColumn<Pedidos, Date> tableColumnFechaPedido;
    @FXML
    TableColumn<Pedidos,Date> tableColumnFechaEntrega;
    @FXML
    TableColumn<Pedidos,String> tableColumnEstado;
    @FXML
    TableColumn<Pedidos,Double> tableColumnTotal;
    @FXML
    TableColumn<Pedidos,String> tableColumnNombreProveedor;

    ObservableList<Pedidos> listaPedidos;
    ObservableList<Proveedores> listaProveedores;
    Conexion conexion;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
    conexion = new Conexion();
    conexion.establecerConexion();




    listaPedidos = FXCollections.observableArrayList();
    Pedidos.llenarInformacionPedidos(conexion.getConnection(),listaPedidos);
    tableViewPedidos.setItems(listaPedidos);

        // Enlazando columnas con atributos
        tableColumnId.setCellValueFactory(new PropertyValueFactory<Pedidos,Integer>("id"));
        tableColumnNombreProveedor.setCellValueFactory(new PropertyValueFactory<Pedidos,String>("nombre"));
        tableColumnFechaPedido.setCellValueFactory(new PropertyValueFactory<Pedidos,Date>("fecha_pedido"));
        tableColumnFechaEntrega.setCellValueFactory(new PropertyValueFactory<Pedidos,Date>("fecha_entraga"));
        tableColumnEstado.setCellValueFactory(new PropertyValueFactory<Pedidos,String>("nomestado"));
        tableColumnTotal.setCellValueFactory(new PropertyValueFactory<Pedidos,Double>("total"));
        gestionarPedidos();
        conexion.cerrarConexion();



    }


    public void gestionarPedidos(){
        tableViewPedidos.getSelectionModel().selectedItemProperty().addListener(
                new ChangeListener<Pedidos>() {
                    @Override
                    public void changed(ObservableValue<? extends Pedidos> arg0, Pedidos valorAnterior, Pedidos valorSeleccionado) {
                        if (valorSeleccionado!=null){
                            textFieldIdP.setText(String.valueOf(valorSeleccionado.getId()));
                            textFielProveedor.setText(String.valueOf(valorSeleccionado.getProveedor_id()));
                            datePickerFechaPedido.setValue(valorSeleccionado.getFecha_pedido().toLocalDate());
                            datePickerFechaEntrega.setValue(valorSeleccionado.getFecha_entraga().toLocalDate());
                            textFieldEstado.setText(String.valueOf(valorSeleccionado.getId_estado()));
                            textFieldTotal.setText(String.valueOf(valorSeleccionado.getTotal()));

                            btnGuardar.setDisable(true);
                            btnEliminar.setDisable(false);
                            btnActualizar.setDisable(false);
                        }
                    }


                }
        );

    }

    @FXML
    public void guardarPedido(){
        Pedidos a = new Pedidos(0,Integer.valueOf(textFielProveedor.getText()),Date.valueOf(datePickerFechaPedido.getValue()),Date.valueOf(datePickerFechaEntrega.getValue()),Integer.valueOf(textFieldEstado.getText()),Double.valueOf(textFieldTotal.getText()) );
        //Llamar al metodo guardarRegistro de la clase Alumno
        conexion.establecerConexion();
        int resultado = a.guardarPedido(conexion.getConnection());
        conexion.cerrarConexion();

        if (resultado == 1){
            listaPedidos.add(a);
            //JDK 8u>40
            Alert mensaje = new Alert(Alert.AlertType.INFORMATION);
            mensaje.setTitle("Registro agregado");
            mensaje.setContentText("El registro pedido ha sido agregado exitosamente");
            mensaje.setHeaderText("Resultado:");
            mensaje.show();
        }

    }

    @FXML
    public void limpiarComponentes(){
        textFieldIdP.setText(null);
        textFielProveedor.setText(null);
        datePickerFechaPedido.setValue(null);
        datePickerFechaEntrega.setValue(null);
        textFieldEstado.setText(null);
        textFieldTotal.setText(null);


        btnGuardar.setDisable(false);
        btnEliminar.setDisable(true);
        btnActualizar.setDisable(true);
    }

    @FXML
    public void eliminarPedido(){
        conexion.establecerConexion();
        int resultado = tableViewPedidos.getSelectionModel().getSelectedItem().eliminarPedido(conexion.getConnection());
        conexion.cerrarConexion();

        if (resultado == 1){
            listaPedidos.remove(tableViewPedidos.getSelectionModel().getSelectedIndex());
            //JDK 8u>40
            Alert mensaje = new Alert(Alert.AlertType.INFORMATION);
            mensaje.setTitle("Registro eliminado");
            mensaje.setContentText("El registro pedido ha sido eliminado exitosamente");
            mensaje.setHeaderText("Resultado:");
            mensaje.show();
        }

    }

    @FXML
    public void actualizarPedido(){
        Pedidos a = new Pedidos(Integer.valueOf(textFieldIdP.getText()),Integer.valueOf(textFielProveedor.getText()),Date.valueOf(datePickerFechaPedido.getValue()),Date.valueOf(datePickerFechaEntrega.getValue()),Integer.valueOf(textFieldEstado.getText()),Double.valueOf(textFieldTotal.getText()));
        conexion.establecerConexion();
        int resultado = a.actualizarPedido(conexion.getConnection());
        conexion.cerrarConexion();

        if (resultado == 1){
            listaPedidos.set(tableViewPedidos.getSelectionModel().getSelectedIndex(),a);
            //JDK 8u>40
            Alert mensaje = new Alert(Alert.AlertType.INFORMATION);
            mensaje.setTitle("Registro actualizado");
            mensaje.setContentText("El registro pedido ha sido actualizado exitosamente");
            mensaje.setHeaderText("Resultado:");
            mensaje.show();
        }

    }
    @FXML
    public void refreshAcccion(){
        conexion.establecerConexion();
        listaPedidos.clear();
        Pedidos.refresT(listaPedidos,conexion.getConnection());
        conexion.cerrarConexion();
    }

}

