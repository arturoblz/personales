package controller;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import modelo.Articulos;
import modelo.ArticulosFamilias;
import modelo.Conexion;

import java.awt.*;
import java.net.URL;
import java.util.ResourceBundle;

public class ArticulosControlador implements Initializable {

    @FXML
    TextField txtId;
    @FXML
    TextField txtCodigo;
    @FXML
    TextField txtFamiliaId;
    @FXML
    TextField txtNombre;
    @FXML
    TextField txtCodigoBarras;
    @FXML
    TextField txtImporte;

    @FXML
    TableView<Articulos> tableViewArticulos;
    @FXML
    TableColumn<Articulos,Integer> tableColumnId;
    @FXML
    TableColumn<Articulos,String> tableColumnCodigo;
    @FXML
    TableColumn<Articulos,Integer> tableColumnFamiliaId;
    @FXML
    TableColumn<Articulos,String> tableColumnNombre;
    @FXML
    TableColumn<Articulos,String> tableColumnCodigoBarras;
    @FXML
    TableColumn<Articulos,Double> tableColumnImporte;
    @FXML
    TableColumn<Articulos,String> tableColumnNombreF;
    @FXML
    Button btnGuardar;
    @FXML
    Button btnEliminar;
    @FXML
    Button btnActualizar;

    private ObservableList<Articulos> listaA;
    private Conexion conexion;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        conexion = new Conexion() ;
        conexion.establecerConexion();
        listaA = FXCollections.observableArrayList();
        Articulos.llenarArticulos(conexion.getConnection(),listaA);
        tableViewArticulos.setItems(listaA);
        tableColumnId.setCellValueFactory(new PropertyValueFactory<Articulos,Integer>("id"));
        tableColumnCodigo.setCellValueFactory(new PropertyValueFactory<Articulos,String>("codigo"));
        tableColumnFamiliaId.setCellValueFactory(new PropertyValueFactory<Articulos,Integer>("familia_id"));
        tableColumnNombre.setCellValueFactory(new PropertyValueFactory<Articulos,String>("nombre"));
        tableColumnCodigoBarras.setCellValueFactory(new PropertyValueFactory<Articulos,String>("codigo_barras"));
        tableColumnImporte.setCellValueFactory(new PropertyValueFactory<Articulos,Double>("importe"));
         gestionarEventos();
        conexion.cerrarConexion();


    }


    public void gestionarEventos(){
        tableViewArticulos.getSelectionModel().selectedItemProperty().addListener(
                new ChangeListener<Articulos>() {
                    @Override
                    public void changed(ObservableValue<? extends Articulos> arg0, Articulos valorAnterior, Articulos valorSeleccionado) {
                        if (valorSeleccionado!=null){

                            txtId.setText(String.valueOf(valorSeleccionado.getId()));
                            txtCodigo.setText(valorSeleccionado.getCodigo());
                            txtFamiliaId.setText(String.valueOf(valorSeleccionado.getFamilia_id()));
                            txtNombre.setText(valorSeleccionado.getNombre());
                            txtCodigoBarras.setText(valorSeleccionado.getCodigo_barras());
                            txtImporte.setText(String.valueOf(valorSeleccionado.getImporte()));


                            btnGuardar.setDisable(true);
                            btnEliminar.setDisable(false);
                            btnActualizar.setDisable(false);
                        }
                    }


                }
        );
    }

    @FXML
    public void guardarArticulo(){
        Articulos a = new Articulos(0,txtCodigo.getText(),Integer.valueOf(txtFamiliaId.getText()),txtNombre.getText(),txtCodigoBarras.getText(),Double.valueOf(txtImporte.getText()));
        //Llamar al metodo guardarRegistro de la clase Alumno
        conexion.establecerConexion();
        int resultado = a.guardarArticulo(conexion.getConnection());
        conexion.cerrarConexion();

        if (resultado == 1){
            listaA.add(a);
            //JDK 8u>40
            Alert mensaje = new Alert(Alert.AlertType.INFORMATION);
            mensaje.setTitle("Registro agregado");
            mensaje.setContentText("El registro ha sido agregado exitosamente");
            mensaje.setHeaderText("Resultado:");
            mensaje.show();
        }

    }
    @FXML
    public void eliminarRegistro(){
        conexion.establecerConexion();
        int resultado = tableViewArticulos.getSelectionModel().getSelectedItem().eliminarArticulos(conexion.getConnection());
        conexion.cerrarConexion();

        if (resultado == 1){
            listaA.remove(tableViewArticulos.getSelectionModel().getSelectedIndex());
            //JDK 8u>40
            Alert mensaje = new Alert(Alert.AlertType.INFORMATION);
            mensaje.setTitle("Registro eliminado");
            mensaje.setContentText("El registro ha sido eliminado exitosamente");
            mensaje.setHeaderText("Resultado:");
            mensaje.show();
        }
    }
    @FXML
    public void actualizarRegistro(){
        Articulos a = new Articulos(Integer.valueOf(txtId.getText()),txtCodigo.getText(),Integer.valueOf(txtFamiliaId.getText()),txtNombre.getText(),txtCodigoBarras.getText(),Double.valueOf(txtImporte.getText()));
        conexion.establecerConexion();
        int resultado = a.actualizarArticulos(conexion.getConnection());
        conexion.cerrarConexion();

        if (resultado == 1){
            listaA.set(tableViewArticulos.getSelectionModel().getSelectedIndex(),a);
            //JDK 8u>40
            Alert mensaje = new Alert(Alert.AlertType.INFORMATION);
            mensaje.setTitle("Registro actualizado");
            mensaje.setContentText("El registro ha sido actualizado exitosamente");
            mensaje.setHeaderText("Resultado:");
            mensaje.show();
        }
    }
    @FXML
    public void limpiarComponentes(){
        txtId.setText(null);
        txtCodigo.setText(null);
        txtFamiliaId.setText(null);
        txtNombre.setText(null);
        txtCodigoBarras.setText(null);
        txtImporte.setText(null);


        btnGuardar.setDisable(false);
        btnEliminar.setDisable(true);
        btnActualizar.setDisable(true);
    }
    @FXML
    public void refreshAcccion(){
        conexion.establecerConexion();
        listaA.clear();
        Articulos.refresT(listaA,conexion.getConnection());
        conexion.cerrarConexion();
    }

}
