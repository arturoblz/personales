package controller;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import modelo.Conexion;
import modelo.Proveedores;
;

import java.net.URL;
import java.util.ResourceBundle;

public class ProveedoresControlador implements Initializable {
    @FXML
    Button btnGuardar;
    @FXML
    Button btnEliminar;
    @FXML
    Button btnActualizar;
    //Etiquetas FXML
    @FXML
    TextField textFieldId;
    @FXML
    TextField textFieldRazonSocial;
    @FXML
    TextField textFieldNombre;
    @FXML
    TextField textFieldNif;
    @FXML
    TextField textFieldDireccion;
    @FXML
    TextField textFieldCodigoPostal;
    @FXML
    TextField textFieldPoblacion;
    @FXML
    TextField textFieldProvincia;
    @FXML
    TextField textFieldPais;
    @FXML
    TextField textFieldTel;
    @FXML
    TextField textFieldMovil;
    @FXML
    TextField textFieldFax;
    @FXML
    TextField textFieldEmail;
    @FXML
    TextField textFieldWeb;
    @FXML
    TextField textFieldActivo;
    @FXML
    TextField textFieldObs;
    @FXML
    TextField textFieldContacto;

    @FXML
    TableView<Proveedores> tableViewProveedores;
    @FXML
    TableColumn<Proveedores,Integer> tableColumnId;
    @FXML
    TableColumn<Proveedores,String> tableColumnRazonSocial;
    @FXML
    TableColumn<Proveedores,String> tableColumnNombre;
    @FXML
    TableColumn<Proveedores,String> tableColumnNif;
    @FXML
    TableColumn<Proveedores,String> tableColumnDireccion;
    @FXML
    TableColumn<Proveedores,String> tableColumnCodigoPostal;
    @FXML
    TableColumn<Proveedores,String> tableColumnPoblacion;
    @FXML
    TableColumn<Proveedores,String> tableColumnProvincia;
    @FXML
    TableColumn<Proveedores,String> tableColumnPais;
    @FXML
    TableColumn<Proveedores,String> tableColumnTel;
    @FXML
    TableColumn<Proveedores,String> tableColumnMovil;
    @FXML
    TableColumn<Proveedores,String> tableColumnFax;
    @FXML
    TableColumn<Proveedores,String> tableColumnEmail;
    @FXML
    TableColumn<Proveedores,String> tableColumnWeb;
    @FXML
    TableColumn<Proveedores,Integer> tableColumnActivo;
    @FXML
    TableColumn<Proveedores,String> tableColumnObs;
    @FXML
    TableColumn<Proveedores,String> tableColumnContacto;

    private ObservableList<Proveedores> listaP;
    private Conexion conexion;
     @Override
    public void initialize(URL location, ResourceBundle resources) {
         conexion = new Conexion() ;
         conexion.establecerConexion();
         listaP = FXCollections.observableArrayList();
         Proveedores.llenarInformacionProveedores(conexion.getConnection(),listaP);
         tableViewProveedores.setItems(listaP);

         // Enlazando columnas con atributos
         tableColumnId.setCellValueFactory(new PropertyValueFactory<Proveedores,Integer>("id"));
         tableColumnRazonSocial.setCellValueFactory(new PropertyValueFactory<Proveedores,String>("razon_social"));
         tableColumnNombre.setCellValueFactory(new PropertyValueFactory<Proveedores,String>("nombre"));
         tableColumnNif.setCellValueFactory(new PropertyValueFactory<Proveedores,String>("nif"));
         tableColumnDireccion.setCellValueFactory(new PropertyValueFactory<Proveedores,String>("direccion"));
         tableColumnCodigoPostal.setCellValueFactory(new PropertyValueFactory<Proveedores,String>("codigo_postal"));
         tableColumnPoblacion.setCellValueFactory(new PropertyValueFactory<Proveedores,String>("poblacion"));
         tableColumnProvincia.setCellValueFactory(new PropertyValueFactory<Proveedores,String>("provincia"));
         tableColumnPais.setCellValueFactory(new PropertyValueFactory<Proveedores,String>("pais"));
         tableColumnTel.setCellValueFactory(new PropertyValueFactory<Proveedores,String>("tel"));
         tableColumnMovil.setCellValueFactory(new PropertyValueFactory<Proveedores,String>("movil"));
         tableColumnFax.setCellValueFactory(new PropertyValueFactory<Proveedores,String>("fax"));
         tableColumnEmail.setCellValueFactory(new PropertyValueFactory<Proveedores,String>("email"));
         tableColumnWeb.setCellValueFactory(new PropertyValueFactory<Proveedores,String>("web"));
         tableColumnActivo.setCellValueFactory(new PropertyValueFactory<Proveedores,Integer>("activo"));
         tableColumnObs.setCellValueFactory(new PropertyValueFactory<Proveedores,String>("obs"));
         tableColumnContacto.setCellValueFactory(new PropertyValueFactory<Proveedores,String>("contacto"));
         gestionarEventosProveedores();



         conexion.cerrarConexion();

    }

    public void gestionarEventosProveedores(){
        tableViewProveedores.getSelectionModel().selectedItemProperty().addListener(
                new ChangeListener<Proveedores>() {
                    @Override
                    public void changed(ObservableValue<? extends Proveedores> arg0, Proveedores valorAnterior, Proveedores valorSeleccionado) {
                        if (valorSeleccionado!=null){
                            textFieldId.setText(String.valueOf(valorSeleccionado.getId()));
                            textFieldRazonSocial.setText(valorSeleccionado.getRazon_social());
                            textFieldNombre.setText(valorSeleccionado.getNombre());
                            textFieldNif.setText(valorSeleccionado.getNif());
                            textFieldDireccion.setText(valorSeleccionado.getDireccion());
                            textFieldCodigoPostal.setText(valorSeleccionado.getCodigo_postal());
                            textFieldPoblacion.setText(valorSeleccionado.getPoblacion());
                            textFieldProvincia.setText(valorSeleccionado.getProvincia());
                            textFieldPais.setText(valorSeleccionado.getPais());
                            textFieldTel.setText(valorSeleccionado.getTel());
                            textFieldMovil.setText(valorSeleccionado.getMovil());
                            textFieldFax.setText(valorSeleccionado.getFax());
                            textFieldEmail.setText(valorSeleccionado.getEmail());
                            textFieldWeb.setText(valorSeleccionado.getWeb());
                            textFieldActivo.setText(String.valueOf(valorSeleccionado.getActivo()));
                            textFieldObs.setText(valorSeleccionado.getObs());
                            textFieldContacto.setText(valorSeleccionado.getContacto());


                            btnGuardar.setDisable(true);
                            btnEliminar.setDisable(false);
                            btnActualizar.setDisable(false);

                        }
                    }


                }
        );
    }
    @FXML
    public void setBtnGuardar(){

        Proveedores a = new Proveedores(0,textFieldRazonSocial.getText(),textFieldNombre.getText(),textFieldNif.getText(),textFieldDireccion.getText(),textFieldCodigoPostal.getText(),textFieldPoblacion.getText(),textFieldProvincia.getText(),textFieldPais.getText(),textFieldTel.getText(),textFieldMovil.getText(),textFieldFax.getText(),textFieldEmail.getText(),textFieldWeb.getText(),Integer.valueOf(textFieldActivo.getText()),textFieldObs.getText(),textFieldContacto.getText());
        //Llamar al metodo guardarRegistro de la clase Alumno
        conexion.establecerConexion();
        int resultado = a.guardarProveedores(conexion.getConnection());
        conexion.cerrarConexion();

        if (resultado == 1){
            listaP.add(a);
            //JDK 8u>40
            Alert mensaje = new Alert(Alert.AlertType.INFORMATION);
            mensaje.setTitle("Registro agregado");
            mensaje.setContentText("El registro proveedores ha sido agregado exitosamente");
            mensaje.setHeaderText("Resultado:");
            mensaje.show();
        }
    }

    @FXML
    public void setBtnActualizar(){
        Proveedores a = new Proveedores(Integer.valueOf(textFieldId.getText()),textFieldRazonSocial.getText(),textFieldNombre.getText(),textFieldNif.getText(),textFieldDireccion.getText(),textFieldCodigoPostal.getText(),textFieldPoblacion.getText(),textFieldProvincia.getText(),textFieldPais.getText(),textFieldTel.getText(),textFieldMovil.getText(),textFieldFax.getText(),textFieldEmail.getText(),textFieldWeb.getText(),Integer.valueOf(textFieldActivo.getText()),textFieldObs.getText(),textFieldContacto.getText());
        conexion.establecerConexion();
        int resultado = a.actualizarProveedores(conexion.getConnection());
        conexion.cerrarConexion();

        if (resultado == 1){
            listaP.set(tableViewProveedores.getSelectionModel().getSelectedIndex(),a);
            //JDK 8u>40
            Alert mensaje = new Alert(Alert.AlertType.INFORMATION);
            mensaje.setTitle("Registro actualizado");
            mensaje.setContentText("El registro proveedores ha sido actualizado exitosamente");
            mensaje.setHeaderText("Resultado:");
            mensaje.show();
        }
    }
    @FXML
        public void eliminarProveedores(){
        conexion.establecerConexion();
        int resultado = tableViewProveedores.getSelectionModel().getSelectedItem().eliminarProveedores(conexion.getConnection());
        conexion.cerrarConexion();

        if (resultado == 1){
            listaP.remove(tableViewProveedores.getSelectionModel().getSelectedIndex());
            //JDK 8u>40
            Alert mensaje = new Alert(Alert.AlertType.INFORMATION);
            mensaje.setTitle("Registro eliminado");
            mensaje.setContentText("El registro proveedoresha sido eliminado exitosamente");
            mensaje.setHeaderText("Resultado:");
            mensaje.show();
        }
    }
    @FXML
    public void limpiarProveedores(){
        textFieldId.setText(null);
        textFieldRazonSocial.setText(null);
        textFieldNombre.setText(null);
        textFieldNif.setText(null);
        textFieldDireccion.setText(null);
        textFieldCodigoPostal.setText(null);
        textFieldPoblacion.setText(null);
        textFieldProvincia.setText(null);
        textFieldPais.setText(null);
        textFieldTel.setText(null);
        textFieldMovil.setText(null);
        textFieldFax.setText(null);
        textFieldEmail.setText(null);
        textFieldWeb.setText(null);
        textFieldActivo.setText(null);
        textFieldObs.setText(null);
        textFieldContacto.setText(null);


        btnGuardar.setDisable(false);
        btnEliminar.setDisable(true);
        btnActualizar.setDisable(true);
    }
    @FXML
    public void refreshAcccion(){
        conexion.establecerConexion();
        listaP.clear();
        Proveedores.refresT(listaP,conexion.getConnection());
        conexion.cerrarConexion();
    }
}
