package controller;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import modelo.*;


import java.net.URL;
import java.util.ResourceBundle;

public class PedidosLineasControlador implements Initializable {
    @FXML
    TextField textFieldIdL;
    @FXML
    ComboBox<Pedidos> comboBoxPedidos;

    @FXML
    ComboBox<Articulos> comboBoxArticulo;
     @FXML
     TextField textFieldArticuloCodigo;
     @FXML
     TextField textFieldArticuloDetalle;
     @FXML
     TextField textFieldCantidad;
     @FXML
     TextField textFieldPrecioUnitario;
     @FXML
     TextField textFieldPrecioTotal;
     @FXML
     TextField textFieldUnidadesServidas;


     @FXML
     Button btnGuardar;
     @FXML
     Button btnEliminar;
     @FXML
     Button btnActualizar;






    @FXML
    TableView<PedidosLineas> tableViewPedidosLineas;
    @FXML
    TableColumn<PedidosLineas, Integer> tableColumnId;
    @FXML
    TableColumn<PedidosLineas, Integer> tableColumnPedidoId;
    @FXML
    TableColumn<PedidosLineas, String> tableColumnArticuloId;
    @FXML
    TableColumn<PedidosLineas, String> tableColumnArticuloCodigo;
    @FXML
    TableColumn<PedidosLineas, String> tableColumnArticuloDetalle;
    @FXML
    TableColumn<PedidosLineas, Integer> tableColumnCant;
    @FXML
    TableColumn<PedidosLineas, Double> tableColumnPrecioUnitario;
    @FXML
    TableColumn<PedidosLineas, Double> tableColumnPrecioTotal;
    @FXML
    TableColumn<PedidosLineas, Integer> tableColumnUnidadesServidas;

    ObservableList<PedidosLineas> listaPedidosLineas;
    ObservableList<Articulos> listaArticulos;
    ObservableList<Pedidos> listaPedidos;

    Conexion conexion;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    conexion = new Conexion();
    conexion.establecerConexion();
    comboBoxPedidos.getSelectionModel().select(1);
        System.out.println(comboBoxPedidos.getSelectionModel().getSelectedIndex());

        listaPedidos = FXCollections.observableArrayList();
        Pedidos.llenarInformacionPedidos(conexion.getConnection(), listaPedidos);
        comboBoxPedidos.setItems(listaPedidos);

        listaArticulos = FXCollections.observableArrayList();
        Articulos.llenarArticulos(conexion.getConnection(), listaArticulos);
        comboBoxArticulo.setItems(listaArticulos);
        listaPedidosLineas = FXCollections.observableArrayList();
    PedidosLineas.llenarInformacionPedidosLineas(conexion.getConnection(),listaPedidosLineas);
    tableViewPedidosLineas.setItems(listaPedidosLineas);
        // Enlazando columnas con atributos
        tableColumnId.setCellValueFactory(new PropertyValueFactory<PedidosLineas,Integer>("id"));
        tableColumnPedidoId.setCellValueFactory(new PropertyValueFactory<PedidosLineas,Integer>("pedido_id"));
        tableColumnArticuloId.setCellValueFactory(new PropertyValueFactory<PedidosLineas,String>("id"));
        tableColumnArticuloCodigo.setCellValueFactory(new PropertyValueFactory<PedidosLineas,String>("articulo_codigo"));
        tableColumnArticuloDetalle.setCellValueFactory(new PropertyValueFactory<PedidosLineas,String>("articulo_detalle"));
        tableColumnCant.setCellValueFactory(new PropertyValueFactory<PedidosLineas,Integer>("cant"));
        tableColumnPrecioUnitario.setCellValueFactory(new PropertyValueFactory<PedidosLineas,Double>("precio_unitario"));
        tableColumnPrecioTotal.setCellValueFactory(new PropertyValueFactory<PedidosLineas,Double>("precio_total"));
        tableColumnUnidadesServidas.setCellValueFactory(new PropertyValueFactory<PedidosLineas,Integer>("unidades_servidas"));
        gestionarEventos();

        conexion.cerrarConexion();


    }


    public void gestionarEventos(){

        tableViewPedidosLineas.getSelectionModel().selectedItemProperty().addListener(
                new ChangeListener<PedidosLineas>() {
                    @Override
                    public void changed(ObservableValue<? extends PedidosLineas> arg0, PedidosLineas valorAnterior, PedidosLineas valorSeleccionado) {
                        if (valorSeleccionado!=null){
                            textFieldIdL.setText(String.valueOf(valorSeleccionado.getId()));
                            //comb's
                            textFieldArticuloCodigo.setText(valorSeleccionado.getArticulo_codigo());
                            textFieldArticuloDetalle.setText(valorSeleccionado.getArticulo_detalle());
                            textFieldCantidad.setText(String.valueOf(valorSeleccionado.getCant()));
                            textFieldPrecioUnitario.setText(String.valueOf(valorSeleccionado.getPrecio_unitario()));
                            textFieldPrecioTotal.setText(String.valueOf(valorSeleccionado.getPrecio_total()));
                            textFieldUnidadesServidas.setText(String.valueOf(valorSeleccionado.getUnidades_servidas()));


                            btnGuardar.setDisable(true);
                            btnEliminar.setDisable(false);
                            btnActualizar.setDisable(false);
                        }
                    }


                }
        );

    }


    @FXML
    public void limpiarPedidosLineas(){
        textFieldIdL.setText(null);
        comboBoxPedidos.setValue(null);
        comboBoxArticulo.setValue(null);
        textFieldArticuloCodigo.setText(null);
        textFieldArticuloDetalle.setText(null);
        textFieldCantidad.setText(null);
        textFieldPrecioUnitario.setText(null);
        textFieldPrecioTotal.setText(null);
        textFieldUnidadesServidas.setText(null);


        btnGuardar.setDisable(false);
        btnEliminar.setDisable(true);
        btnActualizar.setDisable(true);

    }


    @FXML
    public void guardarPedidosLineas(){
        PedidosLineas a = new PedidosLineas(0,0,0,textFieldArticuloCodigo.getText(),textFieldArticuloDetalle.getText(),Integer.valueOf(textFieldCantidad.getText()),Double.valueOf(textFieldPrecioUnitario.getText()),Double.valueOf(textFieldPrecioTotal.getText()),Integer.valueOf(textFieldUnidadesServidas.getText()));
        //Llamar al metodo guardarRegistro de la clase Alumno
        conexion.establecerConexion();
        int resultado = a.guardarLineas(conexion.getConnection());
        conexion.cerrarConexion();

        if (resultado == 1){
            listaPedidosLineas.add(a);
            //JDK 8u>40
            Alert mensaje = new Alert(Alert.AlertType.INFORMATION);
            mensaje.setTitle("Registro agregado");
            mensaje.setContentText("El registro pedidos lineas ha sido agregado exitosamente");
            mensaje.setHeaderText("Resultado:");
            mensaje.show();
        }

    }

    @FXML
    public void elminarPedidosLineas(){

        conexion.establecerConexion();
        int resultado = tableViewPedidosLineas.getSelectionModel().getSelectedItem().eliminarLineas(conexion.getConnection());
        conexion.cerrarConexion();

        if (resultado == 1){
            listaPedidosLineas.remove(tableViewPedidosLineas.getSelectionModel().getSelectedIndex());
            //JDK 8u>40
            Alert mensaje = new Alert(Alert.AlertType.INFORMATION);
            mensaje.setTitle("Registro eliminado");
            mensaje.setContentText("El registro pedidos lineas ha sido eliminado exitosamente");
            mensaje.setHeaderText("Resultado:");
            mensaje.show();
        }
    }

    @FXML
    public void actualizarPedidosLineas(){
        PedidosLineas a = new PedidosLineas(Integer.valueOf(textFieldIdL.getText()),Integer.valueOf(comboBoxPedidos.getSelectionModel().getSelectedItem().getProveedor_id()),Integer.valueOf(comboBoxArticulo.getSelectionModel().getSelectedItem().getId()),textFieldArticuloCodigo.getText(),textFieldArticuloDetalle.getText(),Integer.valueOf(textFieldCantidad.getText()),Double.valueOf(textFieldPrecioUnitario.getText()),Double.valueOf(textFieldPrecioTotal.getText()),Integer.valueOf(textFieldUnidadesServidas.getText()));
        conexion.establecerConexion();
        int resultado = a.actualizarLineas(conexion.getConnection());
        conexion.cerrarConexion();

        if (resultado == 1){
            listaPedidosLineas.set(tableViewPedidosLineas.getSelectionModel().getSelectedIndex(),a);
            //JDK 8u>40
            Alert mensaje = new Alert(Alert.AlertType.INFORMATION);
            mensaje.setTitle("Registro actualizado");
            mensaje.setContentText("El registro pedidos lineas ha sido actualizado exitosamente");
            mensaje.setHeaderText("Resultado:");
            mensaje.show();
        }else {

            System.out.println("NO");
        }

    }
    @FXML
    public void refreshAcccion(){
        conexion.establecerConexion();
        listaPedidosLineas.clear();
        PedidosLineas.refresT(listaPedidosLineas,conexion.getConnection());
        conexion.cerrarConexion();
    }
}
