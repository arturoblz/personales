package tienda;

import java.util.Scanner;

public class Tienda {

    public static void main(String[] args) {
        int matriz[][] = new int[5][5];
        int vector0[] = new int[5];
        int vector1[] = new int[5];
        int vector2[] = new int[5];
        int vector3[] = new int[5];
        int vector4[] = new int[5];
        int ventasTotalesT0 = 0, ventasTotalesT1 = 0, ventasTotalesT2 = 0, ventasTotalesT3 = 0, ventasTotalesT4 = 0;
        int ventasTotalesD0 = 0, ventasTotalesD1 = 0, ventasTotalesD2 = 0, ventasTotalesD3 = 0, ventasTotalesD4 = 0;
        int ventasTotalesTiendas = 0, busca;
        //Ejercicio 1
        //Llenando matriz
        Scanner entrada = new Scanner(System.in);
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                System.out.println("Ingresa el costo del articulo " + i + " de la tienda " + j + ": ");
                matriz[i][j] = entrada.nextInt();
            }
        }
        //Mostrando matriz 
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                System.out.println("El costo del articulo " + i + " de la tienda " + j + " es de : " + matriz[i][j]);
            }
        }
        //Ejercicio 2 
        //Mostando ventas totales del dia para cada tienda
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                if (i == 0) {
                    ventasTotalesT0 = ventasTotalesT0 + matriz[i][j];
                } else if (i == 1) {
                    ventasTotalesT1 = ventasTotalesT1 + matriz[i][j];
                } else if (i == 2) {
                    ventasTotalesT2 = ventasTotalesT2 + matriz[i][j];
                } else if (i == 3) {
                    ventasTotalesT3 = ventasTotalesT3 + matriz[i][j];
                } else {
                    ventasTotalesT4 = ventasTotalesT4 + matriz[i][j];
                }
            }
        }

        //Motrando ventas totales en el dia para cada tienda 
        System.out.println("\n\nLas ventas totales para la tienda 0 son de : " + ventasTotalesT0);
        System.out.println("Las ventas totales para la tienda 0 son de : " + ventasTotalesT1);
        System.out.println("Las ventas totales para la tienda 0 son de : " + ventasTotalesT2);
        System.out.println("Las ventas totales para la tienda 0 son de : " + ventasTotalesT3);
        System.out.println("Las ventas totales para la tienda 0 son de : " + ventasTotalesT4);

        //Ejercicio 3
        //Mostando ventas totales para cada uno de los deportes 
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                if (j == 0) {
                    ventasTotalesD0 = ventasTotalesD0 + matriz[i][j];
                } else if (j == 1) {
                    ventasTotalesD1 = ventasTotalesD1 + matriz[i][j];
                } else if (j == 2) {
                    ventasTotalesD2 = ventasTotalesD2 + matriz[i][j];
                } else if (j == 3) {
                    ventasTotalesD3 = ventasTotalesD3 + matriz[i][j];
                } else {
                    ventasTotalesD4 = ventasTotalesD4 + matriz[i][j];
                }
            }
        }
        //Motrando ventas totales en el dia para cada tienda 
        System.out.println("\n\nLas ventas totales para la tienda 0 son de : " + ventasTotalesD0);
        System.out.println("Las ventas totales para el deporte 0 son de : " + ventasTotalesD1);
        System.out.println("Las ventas totales para el deporte 0 son de : " + ventasTotalesD2);
        System.out.println("Las ventas totales para el deporte 0 son de : " + ventasTotalesD3);
        System.out.println("Las ventas totales para el deporte 0 son de : " + ventasTotalesD4);

        //Ejercicio 4
        //Calculando ventas totales de todas las tiendas
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                ventasTotalesTiendas = ventasTotalesTiendas + matriz[i][j];
            }
        }
        //Mostrando ventas totales de todas las tiendas
        System.out.println("\n\nLas ventas totales de todas las tiendas son de : " + ventasTotalesTiendas);

        //Ordenando matriz
        int temporal2=0;
        for (int i = 0; i < matriz.length; i++) {

            for (int j = 0; j < matriz[i].length; j++) {
                if (j == 0) {
                    vector0[i] = matriz[i][j];
                }
                if (j == 1) {
                    vector1[i] = matriz[i][j];
                }
                if (j == 2) {
                    vector2[i] = matriz[i][j];
                }
                if (j == 3) {
                    vector3[i] = matriz[i][j];
                }
                if (j == 4) {
                    vector4[i] = matriz[i][j];
                }

            }
        }
        
            imprimirVector(vector0); 
            System.out.println("\n");
            imprimirVector(vector1); 
            System.out.println("\n");
            imprimirVector(vector2); 
            System.out.println("\n");
            imprimirVector(vector3); 
            System.out.println("\n");
            imprimirVector(vector4); 
            System.out.println("\n");
//Ordenar vector
        ordenar(vector0);
        ordenar(vector1);
        ordenar(vector2);
        ordenar(vector3);
        ordenar(vector4);
        
            System.out.println("Matriz ordenada");
            System.out.println("\n");
            imprimirVector(vector0); 
            System.out.println("\n");
            imprimirVector(vector1); 
            System.out.println("\n");
            imprimirVector(vector2); 
            System.out.println("\n");
            imprimirVector(vector3); 
            System.out.println("\n");
            imprimirVector(vector4);
            System.out.println("\n");
       

        
        

        System.out.println("Ingresa un numero  a buscar ");
        busca = entrada.nextInt();
        for (int a = 0; a < matriz.length; a++) {
            for (int j = 0; j < matriz[a].length; j++) {
                if (busca == matriz[a][j]) {
                    System.out.println("Dato encontrado en la posicion " + a + " " + j);
                }temporal2 = 1; 
            }
        }
        
        if(temporal2 == 0){   
            System.out.println("Dato no encontrado");
        }
    }
    
    public static void ordenar(int[] vector0){
    int temporal,iteracion=0; //Ayuda a cambiar la posicion de valores 
        for (int i = 0; i < vector0.length; i++) {
              int bandera = 0; 
              
            for (int j = 0; j < vector0.length - 1; j++) {
                //    Comparando valores en las posiciones j 
                if (vector0[j] > vector0[j + 1]) {
                    temporal = vector0[j];
                    vector0[j] = vector0[j + 1];
                    vector0[j + 1] = temporal;
                    bandera =1; 
                }
            }
            if(bandera==0){
                break; 
            }
            iteracion++; 
        }
    }
    
    
        public static void imprimirVector(int[] vector){
            for(int i=0;i<vector.length;i++){
                System.out.println("Tienda "+ i+" "+ vector[i]);
            }
        }
}
//Imprimiendo vector

//Busqueda de dato 

